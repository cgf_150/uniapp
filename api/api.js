import request from '@/utils/request'

export function test1(data) {
    return request({
        method: 'post',
        url: `/app/jigou_user/user_info`,
        params:data,
    })
}
export function test2(data) {
    return request({
        method: 'post',
        url: `/app/jigou_user/user_info`,
        data:data,
    })
}
export function test3(data) {
    return request({
        method: 'get',
        url: `/app/jigou_user/user_info`,
        data:data,
    })
}
