import Vue from 'vue'
import App from './App'

import { _router, parseRoute } from '@/utils/router'
import { showToast } from "@/utils/request"

//导入uview
import uView from "uview-ui";
Vue.use(uView);

//混入$route对象
Vue.mixin({
    onLoad() {
        const { $mp } = this.$root
        this._route = parseRoute($mp)
    },
    onShow() {
        _router.app = this
        _router.currentRoute = this._route
    },
    methods: {
        showToast
    },
})
//在原型上挂载$router方法
Object.defineProperty(Vue.prototype, '$router', {
    get() {
        return _router
    }
})
Object.defineProperty(Vue.prototype, '$route', {
    get() {
        return this._route
    }
})


Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()