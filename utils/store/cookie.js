//判断storeage中是否有值
function _has(key) {
    if (!key) {
        return
    }
    let value = uni.getStorageSync(key)
    if (value) {
        return true
    }
    return false
}
//将data存储在本地storeage中
function set(key, data, time) {
    if (!key) {
        return;
    }
    uni.setStorageSync(key, data)
}
//获取storeage中指定key的内容
function get(key) {
    if (!key || !_has(key)) {
        return '';
    }
    return uni.getStorageSync(key)
}
//删除storeage中指定key的内容
function remove(key) {
    if (!key || !_has(key)) {
        return;
    }
    uni.removeStorageSync(key)
}

//清空storeage中所有内容
function clearAll() {
    uni.clearStorageSync()
}

//获取当前 storage 的相关信息
function all() {
    return uni.getStorageInfoSync()
}


export default {
    _has,
    set,
    get,
    remove,
    clearAll,
    all
}