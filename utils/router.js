import stringify from "@/utils/querystring";
import cookie from "@/utils/store/cookie";

//判断是否登录
export function handleAuth() {

    if (cookie.get('token')) {
        return true
    }
    return false
}


// 路由对象
export function parseRoute($mp) {
    const _$mp = $mp || {}
    const path = _$mp.page && _$mp.page.route
    return {
        path: `/${path}`,
        query: _$mp.query || _$mp.page.options,
        fullPath: parseUrl({
            path: `/${path}`,
            query: _$mp.query || _$mp.page.options
        }),
        name: path && path.replace(/\/(\w)/g, ($0, $1) => $1.toUpperCase()),
        page: getCurrentPages()
    }
}



//获取带参数的url
export function parseUrl(location) {
    if (typeof location === 'string') return location
    const {
        path,
        query
    } = location

    const queryStr = stringify(query)

    if (!queryStr) {
        return path
    }

    return `${path}?${queryStr}`;
}

//处理路由跳转
export const handleLoginStatus = (location, complete, fail, success) => {
    // 不登录可访问的页面
    let page = [
    {
        path: '/pages/TabBar/Home/index',
        name: '首页'
    },
    {
        path: '/pages/Login/index',
        name: '登录页'
    }
    ]

    // 是否可以访问
    let isAuth = false

    let path = ''
    if (typeof location === 'string') {
        path = location
    } else {
        path = location.path
    }

    // 判断用户是否有token
    if (!handleAuth()) {
        page.map((item) => {
            if (item.path == path) {
                isAuth = true
            }
        })
    } else {
        isAuth = true
    }

    return new Promise((resolve, reject) => {

        if (isAuth) {
            // 有token
            resolve({
                url: parseUrl(location),
                complete,
                fail,
                success
            })
        } else {
            // 没有token，跳转登录页
            uni.navigateTo({
                url: '/pages/Login/index'
            });

            reject('未登录,跳转授权页')
        }
    }).catch(error => {
        console.log(error)
    })
}

//普通跳转
export function navigateTo(location, complete, fail, success) {
    handleLoginStatus(location, complete, fail, success).then(params => {
        uni.navigateTo(params)
    }).catch(error => {
        // 没有权限

    })
}
//重定向跳转
export function redirectTo(location, complete, fail, success) {
    handleLoginStatus(location, complete, fail, success).then(params => {
        uni.redirectTo(params)
    }).catch(error => {
        // 没有权限

    })
}
// 关闭当前页跳转
export function reLaunch(location, complete, fail, success) {
    handleLoginStatus(location, complete, fail, success).then(params => {
        console.log(params)
        uni.reLaunch(params)
    }).catch(error => {
        // 没有权限
    })
}
// 返回
export function navigateBack(delta) {
    uni.navigateBack({
        delta
    })
}

// 跳转tabbar页
export function switchTab(location, complete, fail, success) {
    handleLoginStatus(location, complete, fail, success).then(params => {
        uni.switchTab(params)
    }).catch(error => {
        // 没有权限
    })
}


export const _router = {
    navigateTo,
    redirectTo,
    reLaunch,
    switchTab,
    navigateBack
}